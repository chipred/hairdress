package Utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import dtos.UserDTO;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;

public class UserMapper {
    public static String toJson(List<UserDTO> userDTOList) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writerWithDefaultPrettyPrinter()
        .writeValueAsString(userDTOList);
    }
}
