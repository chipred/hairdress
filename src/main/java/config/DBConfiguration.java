package config;

import java.sql.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;

public class DBConfiguration {

    private final Logger log = LogManager.getLogger(DBConfiguration.class);
    private Statement stmt = null;
    private ResultSet rs = null;
    private Connection conection;

    public DBConfiguration() {
        this.conection = connection();
    }

    /**
     * Stablishing connection with mysql database
     *
     * @return Connection connection
     */
    public Connection connection() {
        Connection conn = null;

        try {

            final String url = "jdbc:mysql://localhost:3306/hairdresser?useSSL=false";
            final String username = "root";
            final String password = "";
            conn = DriverManager.getConnection(url, username, password);

        } catch (SQLException ex) {
            log.error(String.format("SQLException: %s \nSQLState: %s \nVendorError: %s", ex.getMessage(), ex.getSQLState(), ex.getErrorCode()));
        }
        return conn;
    }

    public ResultSet createConsult(final String query) throws SQLException {
        stmt = getConnection().createStatement();
        if (stmt.execute(query)) {
            rs = stmt.getResultSet();
        }
        return rs;
    }

    public void closeConnection() {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException sqlEx) {
                rs = null;
            }
        }

        if (stmt != null) {
            try {
                stmt.close();
            } catch (SQLException ex) {
                log.error(String.format("SQLException: %s \nSQLState: %s \nVendorError: %s", ex.getMessage(), ex.getSQLState(), ex.getErrorCode()));
            }

            stmt = null;
        }
    }

    /**
     * @return the conection
     */
    public Connection getConnection() {
        return conection;
    }
}
