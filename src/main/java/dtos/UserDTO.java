package dtos;

public class UserDTO {

    private String firstName;
    private String lastName;
    private String userName;
    private String pass;

    public UserDTO() {
    }

    public UserDTO(final String firstName, final String lastname, final String userName, final String pass) {
        this.firstName = firstName;
        this.lastName = lastname;
        this.userName = userName;
        this.pass = pass;
    }

    /**
     * @return the name
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName the name to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the password
     */
    public String getPass() {
        return pass;
    }

    /**
     * @param pass the password to set
     */
    public void setPass(String pass) {
        this.pass = pass;
    }

    /**
     * @return the lastname
     */
    public String getLastname() {
        return lastName;
    }

    /**
     * @param lastname the lastname to set
     */
    public void setLastname(String lastname) {
        this.lastName = lastname;
    }

    /**
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName the userName to set
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }
}
