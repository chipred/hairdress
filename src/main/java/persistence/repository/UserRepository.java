package persistence.repository;

import config.DBConfiguration;
import dtos.UserDTO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserRepository {

    public DBConfiguration dbConfiguration = new DBConfiguration();

    public List<UserDTO> getRows(final String query) {
        final List<UserDTO> rows = new ArrayList<>();
        final ResultSet result;
        try {
            result = dbConfiguration.createConsult(query);
            while (result != null && result.next()) {
                UserDTO user = new UserDTO();
                user.setUserName(result.getString("user_name"));
                user.setFirstName(result.getString("first_name"));
                user.setLastname(result.getString("last_name"));
                rows.add(user);
            }
        } catch (SQLException ex) {
            throw new RuntimeException(String.format("SQLException: %s \nSQLState: %s \nVendorError: %s", ex.getMessage(), ex.getSQLState(), ex.getErrorCode()));
        }

        return rows;
    }
}
