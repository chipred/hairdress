package persistence.store;

import dtos.UserDTO;
import persistence.repository.UserRepository;

import java.sql.SQLException;
import java.util.List;

public class UserStore {

    public UserRepository userRepository;

    public List<UserDTO> selectUser() throws SQLException {
        final String query = "Select * from users";
        return userRepository.getRows(query);
    }

    public void createUser(final UserDTO user) throws SQLException {
        final String query = String.format("insert into users (first_name, last_name, user_name, pass, active, created_at, updated_at)\n" +
                "value ('%s','%s','%s','%s','%b',sysdate(),sysdate())", user.getFirstName(), user.getLastname(), user.getUserName(), user.getPass(),true);
    }

    public void updateUser(final UserDTO user)  {
        String query = String.format("Select * from users where user_name='%s'",user.getUserName());
        UserDTO userResponse = userRepository.getRows(query).stream().findFirst().get();
        query = String.format("update users set first_name='%s', last_name='%s',updated_at=sysdate() where user_name='%s'", user.getFirstName(), user.getLastname(), user.getUserName());
        userRepository.getRows(query);
    }

    public void deleteUser(final UserDTO user) {
        final String query = String.format("update users set active='%b', updated_at=sysdate() where user_name='%s'", 0, user.getUserName());
        userRepository.getRows(query);

    }

    public List<UserDTO> findUser(final String user, final String pass) {
        final String query = String.format("Select * from users where user_name='%s'and pass='%s'", user, pass);
        return userRepository.getRows(query);

    }
}
