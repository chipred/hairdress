package services;

import persistence.store.UserStore;

public class LoginService {

    UserStore userStore = new UserStore();

    public Boolean authentication(final String user, final String pass) {
        Boolean usercheck = userStore.findUser(user, pass).stream()
                .filter(userDB -> userDB.getUserName().equals(user))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("username or password is incorrect"))
                .getPass()
                .equals(pass);
        if (!usercheck) {
            throw new RuntimeException("username or password is incorrect");
        }
        return usercheck;

    }
}
