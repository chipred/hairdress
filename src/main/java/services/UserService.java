package services;

import Utils.UserMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import dtos.UserDTO;

import java.util.Arrays;

public class UserService {

    public String getAll() throws JsonProcessingException {
        return UserMapper.toJson(Arrays.asList(new UserDTO("Daniela","Rivas","danir","123.afg"),new UserDTO("Dulcire","Chipre","dchipre","123.afg"),new UserDTO("Denisse","Chipre","dchiprerivas","123.afg")));
    }
}
